﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

	void Update () {
		if (Input.GetKey (KeyCode.R)) {
			print ("R key was pressed");
			transform.Rotate (Vector3.up);
		} else if (Input.GetKey (KeyCode.E)) {
			print ("E key was pressed");
			transform.Rotate (Vector3.down);
		}
	}
}
