﻿using UnityEngine;

public class Spray : MonoBehaviour
{
	void Update() {
		ParticleSystem extinguisherSpray = GetComponent<ParticleSystem>();
		if (Input.GetKeyDown(KeyCode.Q)) { //or D-pad top button on samsung gear 360 controller
			if (extinguisherSpray.isPlaying) {
				extinguisherSpray.Stop ();
			} else {
				extinguisherSpray.Play ();
			}
		}
	}
}